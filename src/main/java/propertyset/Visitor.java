/*
 * Copyright (C) 2003 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Property Set package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
package propertyset;
/**
 * @author andreasm
 */
public interface Visitor
{
	public static final int LEAF = 0;
	public static final int DEPTH_FIRST = 1;
	public static final int BREADTH_FIRST = 2;
    public static final int END = 3;
	/**
	 * Method enter
	 * @param propertyset
	 * @return int
	 * @throws propertyset.VisitorException
	 */
	public int enter(PropertySet propertyset) throws VisitorException;
	/**
	 * Method leave
	 * @param propertyset
	 * @throws propertyset.VisitorException
	 */
	public void leave(PropertySet propertyset) throws VisitorException;
}
