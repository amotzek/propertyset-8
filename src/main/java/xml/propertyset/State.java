/*
 * Copyright (C) 2003, 2007, 2014 Andreas Motzek andreas-motzek@t-online.de
 *
 * This file is part of the Property Set package.
 *
 * You can use, redistribute and/or modify this file under the terms of the Modified Artistic License.
 * See http://simplysomethings.de/open+source/modified+artistic+license.html for details.
 *
 * This file is distributed in the hope that it will be useful, but without any warranty; without even
 * the implied warranty of merchantability or fitness for a particular purpose.
 */
package xml.propertyset;
/*
 * Created on 28.07.2003
 */
import propertyset.Attributes;
import propertyset.PropertySet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
/**
 * @author Andreasm
 */
final class State
{
    private final State parent;
    private final PropertySet propertyset;
    private String defaultnamespace;
    private HashMap<String, String> namespaces;
    //
	public State(State parent)
	{
	    super();
        //
        this.parent = parent;
        //
        propertyset = new PropertySet();
    }
    //
    public void setTag(Tag tag)
    {
        setDefaultNamespace(tag);
        setNamespaces(tag);
        setNameAndNamespace(tag);
        setAttributes(tag);
    }
    //
    private void setDefaultNamespace(Tag tag)
    {
        Map<String, String> attributes = tag.getAttributes();
        //
        if (attributes.containsKey("xmlns"))
        {
            defaultnamespace = attributes.get("xmlns");
        }
        else if (parent != null)
        {
            defaultnamespace = parent.defaultnamespace;
        }
    }
    //
    private void setNamespaces(Tag tag)
    {
        namespaces = new HashMap<>();
        Map<String, String> map = tag.getAttributes();
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        //
        while (iterator.hasNext())
        {
            Map.Entry<String, String> entry = iterator.next();
            String attributename = entry.getKey();
            String attributevalue = entry.getValue();
            //
            if (attributename.startsWith("xmlns:"))
            {
                String namespacename = attributename.substring(6);
                namespaces.put(namespacename, attributevalue);
                iterator.remove();
            }
        }
    }
    //
    private void setNameAndNamespace(Tag tag)
    {
        String namespace = getNamespace(tag);
        String name = getName(tag);
        propertyset.setNamespace(namespace);
        propertyset.setName(name);
    }
    //
    private String getNamespace(Tag tag)
    {
        String tagname = tag.getName();
        int colonindex = tagname.indexOf(':');
        String namespace = defaultnamespace;
        //
        if (colonindex >= 0)
        {
            String namespacename = tagname.substring(0, colonindex);
            namespace = findNamespace(this, namespacename);
        }
        //
        return namespace;
    }
    //
    private static String getName(Tag tag)
    {
        String tagname = tag.getName();
        int colonindex = tagname.indexOf(':');
        //
        if (colonindex < 0) return tagname;
        //
        return tagname.substring(colonindex + 1);
    }
    //
    private static String findNamespace(State state, String namespacename)
    {
        while (state != null)
        {
            String namespace = state.namespaces.get(namespacename);
            //
            if (namespace != null) return namespace;
            //
            state = state.parent;
        }
        //
        return null;
    }
    //
    private void setAttributes(Tag tag)
    {
        Map<String, String> attributemap = tag.getAttributes();
        //
        if (attributemap.isEmpty()) return;
        //
        Attributes attributes = new Attributes();
        //
        for (Map.Entry<String, String> entry : attributemap.entrySet())
        {
            String attributename = entry.getKey();
            String attributevalue = entry.getValue();
            int colonindex = attributename.indexOf(':');
            String namespace = defaultnamespace;
            String name = attributename;
            //
            if (colonindex >= 0)
            {
                String namespacename = attributename.substring(0, colonindex);
                namespace = findNamespace(this, namespacename);
                //
                if (namespace != null) name = attributename.substring(colonindex + 1);
            }
            //
            attributes.setAttribute(namespace, name, attributevalue);
        }
        //
        propertyset.setAttributes(attributes);
    }
    //
    public State getParent()
    {
        return parent;
    }
	//
	public boolean matches(Tag tag)
	{
        String namespace = getNamespace(tag);
        String name = getName(tag);
        //
		return propertyset.hasNamespace(namespace) && propertyset.hasName(name);
	}
	//
	public PropertySet getPropertySet()
	{
		return propertyset;
	}
	//
	public void addChild(PropertySet child)
	{
		propertyset.addChild(child);
	}
	//
	public void setValue(String value)
	{
		propertyset.setValue(value);
	}
}